# Repo for testing gitlab pages

This repo also tests the bounds of the gitlab repo size, we simply create large files using `fallocate` and then add them via lfs.

``` sh
# Install the pre and post commit lfs hooks
git lfs install
# Manually fetch the files once
git lfs pull
# Add new file to lfs tracking
git lfs track big-file.mp4
```

and also the bounds of file sizes